#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;

template <typename T>
void print_container(const T & cont)
{
    for (auto v : cont)
    {
        cout << v;
    }
    cout << endl;
}

template <typename T>
T CountingSort(T & vec)
{
    // determine length of vector
    int n(*max_element(vec.begin(),vec.end()));
    T V(n+1,0);
    T R(vec.size(),0);

    // count elements
    for (size_t i = 0; i < vec.size(); ++i)
        V[vec[i]] += 1;

    // sort
    int k(0);
    for (size_t i = 0; i < V.size(); ++i)
    {
        for (size_t j = 0; j < V[i]; ++j)
            R[k++] = i;
    }
    return R;
}


int main()
{
    vector<int> vec{6,5,4,4,3,2,1,2,3,9,5,5};

    cout << "Size of container: " << vec.size() << endl;
    cout << "unsorted vector:\n";
    print_container(vec);
    cout << "sorted vector:\n";
    print_container(CountingSort(vec));
    return 0;
}
