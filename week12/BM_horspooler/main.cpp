#include <iostream>
#include "include/BMH.h"
#include <chrono>
#include <ctime>
#include <iomanip>
#include <fstream>

using namespace std;

std::map<std::string,std::string> read_fasta(std::string infile)
{
    std::ifstream input(infile);
    if(!input.good()){
        throw std::invalid_argument("Cannot open file.");
    }
    std::map<std::string,std::string> library;
    std::string line(""), name(""), content("");
    while(std::getline( input, line ).good()){
        if (line[0] == '>')
        {
            name = line.substr(1);
            if (content.size() > 0)
            {
                library[name] = content;
                content = "";
            }
        }
        else if (line.empty())
        {
            if (content.size() > 0)
            {
                library[name] = content;
                content.clear();
            }
            break;
        }
        else
        {
            content += line;
        }
    }
    if (!content.empty())
        library[name] = content;
    return library;
}

int main()
{
    // small example:
    std::string ex_s("ACGTAAACGTA");
    std::string ex_p("ACGT"); // should occur 2 times
    BMH ex_bmh(ex_p);
    auto ex_Q = ex_bmh.match(ex_s);
    std::cout << "mini example of " << ex_p << " in " << ex_s << ":\n";
    while (!ex_Q.empty())
    {
        std::cout << "found pattern at index " << ex_Q.front() << "\n";
        ex_Q.pop();
    }

    size_t sizes[6] = {100,1000,10000,100000,500000,1000000};

    std::string path_ref("/home/memsonmi/Desktop/focus_areas/week12/chr4_drosophila/reference.fasta");
    std::map<std::string,std::string> M = read_fasta(path_ref);
    std::cout << "reference file read.\n";
    std::map<std::string,std::string>::iterator it = M.begin();
    std::string s(it->second);

    std::string path_reads("/home/memsonmi/Desktop/focus_areas/week12/chr4_drosophila/reads.fasta");
    std::map<std::string,std::string> R = read_fasta(path_reads);
    std::cout << "reads file read.\n";

    std::clock_t c_start = std::clock();
    auto t_start = std::chrono::high_resolution_clock::now();

    for (size_t j(0); j < 6; j++)
    {
    size_t counter(0);
    for (std::map<std::string,std::string>::iterator itr = R.begin(); itr != R.end(); itr++)
    {
        if (counter > sizes[j])
        {
            break;
        }
        std::string p(itr->second);
        BMH bmh(p);
        auto Q = bmh.match(s);

        counter++;
/*
        while (!Q.empty())
        {
            std::cout << Q.front() << std::endl;
            Q.pop();
        }
*/
        //std::cout << std::endl;
    }
    std::clock_t c_end = std::clock();
    auto t_end = std::chrono::high_resolution_clock::now();
    std::cout << sizes[j] << " reads searched in "
              << std::fixed << std::setprecision(2) << "CPU time used: "
              << 1000.0 * (c_end-c_start) / CLOCKS_PER_SEC << " ms\n"
              << "Wall clock time passed: "
              << std::chrono::duration<double, std::milli>(t_end-t_start).count()
              << " ms\n";
    }
    return 0;
}
