#include "../include/BMH.h"
#include <iostream>


BMH::BMH(std::string & pattern) : N(pattern.size()), P(pattern)
{
    BMH::create_pattern();
}

BMH::~BMH()
{
    //dtor
}

void BMH::create_pattern()
{
    std::map<char , int >::iterator itr;

    for(size_t i = 0; i < N-1; i++)
        M[P[i]] = N - i -1;
    // last char either keeps value or is assigned length N of pattern.
    auto search = M.find(P[N-1]);
    if (search == M.end()) M[P[N-1]] = N;
}

uint16_t BMH::val(char c)
{
    auto search = M.find(c);
    if (search != M.end()) return search->second;
    else return N;
}

std::queue<size_t> BMH::match(std::string & S)
{
    // start with P and S
    // main loop. jumps over i.
    std::queue<size_t> Q;
    for (size_t i(0); i < S.size(); i++)
    {
        uint16_t last_match(val(S[i+N-1])); // calc each time? speedup: check S[i+N-1]==P[N-1] before.
        for (size_t j(1); j <= N; j++)
        {
            if (P[N-j] != S[i+(N-j)]) // match
            {
                i += last_match-1;
                break;
            }
            // comment out for performnce test
            if (j == N) Q.push(i);
        }
    }
    return Q;
}

