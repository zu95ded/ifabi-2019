#ifndef BMH_H
#define BMH_H
#include <map>
#include <string>
#include <algorithm>
#include <queue>

class BMH
{
    uint16_t N; // size of pattern
    std::string P; // actual pattern
    std::map<char, uint16_t> M; // dictionary of characters in pattern

    public:
        BMH(std::string & pattern);
        virtual ~BMH();
        std::queue<size_t> match(std::string & S);

        uint16_t val(char c); // eig private
    private:
        void create_pattern();
};

#endif // BMH_H
