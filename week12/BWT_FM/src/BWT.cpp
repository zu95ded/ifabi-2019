#include "../include/BWT.hpp"

std::string BWT::bwt_construction(std::string const & text)
{
    std::string bwt{};
    std::vector<uint64_t> sa(text.size() + 1);
    std::iota(sa.begin(), sa.end(), 0);
    std::sort(sa.begin(), sa.end(), [&text](uint64_t a, uint64_t b) -> bool
    {
        while (a < text.size() && b < text.size() && text[a] == text[b])
        {
            ++a, ++b;
        }
        if (b == text.size())
            return false;
        if (a == text.size())
            return true;
        return text[a] < text[b];
    });
    for (auto x : sa)
    {
        if (!x)
            bwt += "$";
        else
            bwt += text[x-1];
    }
    return bwt;
}
