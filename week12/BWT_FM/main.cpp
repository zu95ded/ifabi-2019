#include <iostream>
#include "include/BWT.hpp"
#include "bitvector.h"
#include "include/util.hpp"
#include <chrono>


//using namespace std;

int main()
{
    // toy example
    std::string ex_s("ACGTAAACGTA");
    std::string ex_p("ACGT"); // should occur 2 times
    std::cout << "starting toy example. Find " << ex_p << " in " << ex_s << ":\n";
    std::string ex_bwt = BWT::bwt_construction(ex_s);
    std::vector<uint16_t> ex_C = compute_count_table(ex_bwt);
    occurrence_table ex_Occ(ex_bwt,64,512);


    std::cout << count(ex_p, ex_bwt, ex_C, ex_Occ) << " occurrences found." <<'\n'; // prints 2

    //=========================================================================
    std::string lib_path("/home/memsonmi/Desktop/focus_areas/week12/chr4_drosophila/reads.fasta");

    std::map<std::string,std::string> library = read_fasta(lib_path);
    //std::cout << "library read.\n";

    std::string path("/home/memsonmi/Desktop/focus_areas/week12/chr4_drosophila/reference.full.fasta");
    std::map<std::string,std::string> chr = read_fasta(path);
    //std::cout << "file read.\n";

    std::map<std::string,std::string>::iterator it = chr.begin();
    std::string chr4(it->second);
    //std::cout << "string stored\n";
    // ------------ start benchmarking after files were read ------------------
    // construction of BWT & FM index included.

    std::vector<size_t> num_reads{100,1000,10000,100000,500000,1000000};

    for (size_t nr : num_reads)
    {
        auto t_start = std::chrono::high_resolution_clock::now();

        std::string bwt(BWT::bwt_construction(chr4));
        //std::cout << "BWT constructed.\n";// << bwt << std::endl;
        std::vector<uint16_t> C = compute_count_table(bwt);
        //std::cout << "count table computed.\n";
        occurrence_table Occ = occurrence_table(bwt);
        //std::cout << "occurrence table created.\n";

        // --------------------- searching ----------------------------------------
        std::map<std::string,std::string>::iterator itr = library.begin();
        size_t c = 0;
        while (itr != library.end() && (c++) < nr-1)
        {
            std::string P(itr->second);
            count(P,bwt,C,Occ);
            itr++;
        }
        auto t_end = std::chrono::high_resolution_clock::now();
        std::cout << "searched " << (c--) << " reads.\n";
        std::cout << "Time needed: " << std::chrono::duration<double, std::milli>(t_end-t_start).count() << " ms." << std::endl;
        //std::cout << "------------------------------------------------------------------\n";
    }
    return 0;
}
