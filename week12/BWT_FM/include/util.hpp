#ifndef UTIL_HPP_INCLUDED
#define UTIL_HPP_INCLUDED
#pragma once
#include <map>
#include <stdexcept>
#include <stdint.h>
#include <string>
#include <vector>
#include <fstream>
#include "bitvector.h"
#include <iostream>

size_t to_index(char const chr)
{
    switch (chr)
    {
        case '$': return 0;
        case 'A': return 1;
        case 'C': return 2;
        case 'G': return 3;
        case 'T': return 4;
        default: return 5;
    }
}

std::map<std::string,std::string> read_fasta(std::string infile)
{
    std::ifstream input(infile);
    if(!input.good()){
        throw std::invalid_argument("Cannot open file.");
    }
    std::map<std::string,std::string> library;
    std::string line(""), name(""), content("");
    while(std::getline( input, line ).good()){
        if (line[0] == '>')
        {
            name = line.substr(1);
            if (content.size() > 0)
            {
                library[name] = content;
                content = "";
            }
        }
        else if (line.empty())
        {
            if (content.size() > 0)
            {
                library[name] = content;
            }
            break;
        }
        else
        {
            content += line;
        }
        library[name] = content;
    }
    return library;
}

std::vector<uint16_t> compute_count_table(std::string const & bwt)
{
    std::vector<uint16_t> count_table(6);
    for (auto chr : bwt)
    {
        for (size_t i = to_index(chr) + 1; i <= 6; ++i)
            ++count_table[i];
    }
    return count_table;
}

struct occurrence_table
{
    // The list of bitvectors:
    std::vector<Bitvector> data;
    // We want that 5 bitvectors are filled depending on the bwt,
    // so let's customise the constructor of occurrence_table:
    occurrence_table(std::string const & bwt, size_t b = 64, size_t s = 512)
    {
        // resize the 5 bitvectors to the length of the bwt:
        //data.resize(6, Bitvector((bwt.size() + 63)/ 64)); // why? Bitvector constructor already does this.
        data.resize(6, Bitvector(bwt.size()));
        // fill the bitvectors
        for (size_t i = 0; i < bwt.size(); ++i)
            data[to_index(bwt[i])].write(i, 1);
        for (Bitvector & bitv : data)
            bitv.construct(b, s);
    }
    size_t read(char const chr, size_t const i) const
    {
        return data[to_index(chr)].rank(i + 1);
    }
};

size_t count(std::string const & P,
             std::string const & bwt,
             std::vector<uint16_t> const & C,
             occurrence_table const & Occ)
{
    int64_t i = P.size() - 1;
    size_t a = 0;
    size_t b = bwt.size() - 1;
    while ((a <= b) && (i >= 0))
    {
        char c = P[i];
        a = C[to_index(c)] + (a ? Occ.read(c, a - 1) : 0);
        b = C[to_index(c)] + Occ.read(c, b) - 1;
        i = i - 1;
    }
    if (b < a)
        return 0;
    else
        return (b - a + 1);
}

#endif // UTIL_HPP_INCLUDED
