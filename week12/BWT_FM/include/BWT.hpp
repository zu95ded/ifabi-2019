#ifndef BWT_HPP_INCLUDED
#define BWT_HPP_INCLUDED
#pragma once
#include <algorithm>
#include <vector>
#include <string>
#include <stdexcept>
#include <numeric>


struct BWT
{
    static std::string bwt_construction(std::string const & text);
};


#endif // BWT_HPP_INCLUDED
