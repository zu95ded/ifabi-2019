#ifndef BITVECTOR_H
#define BITVECTOR_H
#pragma once
#include <vector>
#include <stdint.h>

class Bitvector
{
public:
    std::vector<uint64_t> data;
    std::vector<uint16_t> blocks;
    std::vector<uint64_t> superblocks;
    uint64_t block_size;
    uint64_t superblock_size;
    Bitvector(size_t const count);
    bool read(size_t const i) const;
    void write(size_t const i, bool const value);
    void construct(size_t const new_block_size, size_t const new_superblock_size);
    uint64_t rank(size_t const i) const;
};
#endif // BITVECTOR_H
