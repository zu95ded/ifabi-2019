#include <iostream>
#include <string>
#include "sa.hpp"
#include <vector>
#include <cstring>
#include <exception>

int main(int argc, char* argv[])
{
	try
	{
		if (argc == 2)
		{
			std::vector<uint32_t> sa;
			construct(sa, argv[1]);
			printc(sa);
			std::cout << "Print LCP array:\n";
			std::vector<uint32_t>lcp = LCP(argv[1], sa);
			printc(lcp);
		}
		else if (argc > 2)
		{
			std::vector<uint32_t> sa;
			sa.reserve(std::strlen(argv[1]));
			construct(sa, argv[1]);
			printc(sa);
			//----durch alle querys:
			std::vector<uint32_t> hits;
			hits.reserve(128);
			for (size_t q = 2; q < argc; q++)
			{
				std::cout << argv[q] << ": ";
				hits.clear();
				find(hits, argv[q], sa, argv[1]);
				std::sort(hits.begin(), hits.end());
				printc(hits);
			}
			std::cout << "Print LCP array:\n";
			std::vector<uint32_t>lcp = LCP(argv[1], sa);
			printc(lcp);
		}
	}
	catch (const std::exception& e)
	{
		std::cout << "unexpected input." << std::endl;
		return 1;
	}



	return 0;
}
