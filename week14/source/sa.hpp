#pragma once // include guard

#include <algorithm>
#include <string>
#include <vector>
#include <iostream>
#include "sa.hpp"
#include <unordered_map>

//=============================================================================
//
//=============================================================================

/// Build suffix array from text.
/// Input: the text (might be empty)
/// Output: a suffix array (sorted). The variable is cleared before its filled and returned.
void construct(std::vector<uint32_t>& sa, const std::string& text);

/// Search a string in a text via suffix array. If the query is empty, return empty hits.
/// Input: search for a 'query' string in a suffix array 'sa' build from 'text'.
/// Output: Results are returned in 'hits'. The variable is cleared before its filled and returned.
void find(std::vector<uint32_t>& hits, const std::string& query, const std::vector<uint32_t>& sa, const std::string& text);

/// kasai's algorithm from
/// https://www.geeksforgeeks.org/%C2%AD%C2%ADkasais-algorithm-for-construction-of-lcp-array-from-suffix-array/
std::vector<uint32_t> LCP(const std::string& txt, std::vector<uint32_t>& suffixArr);

/// helper function to print a container
template <typename T>
void printc(const T& v)
{
  std::cout << *v.begin();
	for (auto it = v.begin(); it != v.end(); it++)
    std::cout << ' ' << *it;
  std::cout << '\n';
}

bool less_than_Subs(uint32_t a, uint32_t b, const std::string& text)
//ist pr�fix text[a] kleiner pr�fix text[b]? Hilfsfunktion f�r construct()
{
	if (text.length() > 0 && a < text.length() && b < text.length())
	{
		while (true)
		{
			if (text[a] < text[b])
				return true;
			else if (text[a] > text[b])
				return false;
			else
			{
				//if ((a + 1) >= text.length() && (b + 1) >= text.length())
				//	return false;
				if ((a + 1) >= text.length())
					return true;
				else if ((b + 1) >= text.length())
					return false;
				else
				{
					++a; ++b;
				}
			}
		}
	}
	else
		return false;
}

bool less_than_Subs(uint32_t a, const std::string& p, const std::string& text)
//ist p kleiner pr�fix von text[a]? Hilfsfunktion f�r binSearch
{
	if (p == "")
		return false;
	if (p.length() > 0 && text.length() > 0 && a < text.length())
	{
		uint32_t b = 0;
		while (true)
		{
			if (p[b] < text[a])
				return true;
			else if (p[b] > text[a])
				return false;
			else // wenn p[b] == text[a]
			{
				if ((a + 1) >= text.length() && (b + 1) >= p.length())
					return false;
				else if ((a + 1) >= text.length())
					return false;
				else if ((b + 1) >= p.length())
					return true;
				else
				{
					++a; ++b;
				}
			}
		}
	}
	else
		return false;
}

bool greater_than_Subs(uint32_t a, const std::string& p, const std::string& text)
//ist p groesser suffix(text[a]) Hilfsfunktion f�r binSearch
{
	if (p == "")
		return false;
	if (p.length() > 0 && text.length() > 0 && a < text.length())
	{
		uint32_t b = 0;
		while (true)
		{
			if (p[b] > text[a]) // p = Q, SA[M] = text[a]
				return true;
			else if (p[b] < text[a])
				return false;
			else
			{
				if ((a + 1) >= text.length() && (b + 1) >= p.length())
					return false;
				else if ((a + 1) >= text.length())
					return true;
				else if ((b + 1) >= p.length())
					return false;
				else
				{
					++a; ++b;
				}
			}
		}
	}
	else
		return false;
}

bool is_prefix(uint32_t a, const std::string& p, const std::string& text)
{
	if (text.length() - a < p.length())
		return false;
	for (size_t i = 0; i < p.size(); i++)
	{
		if (p[i] == text[a])
		{
			++a;
		}
		else
			return false;
	}
	return true;
}

uint32_t binSearch_L(const std::string& p, const std::vector<uint32_t>& sa, const std::string& text)
{
	uint32_t M, n = sa.size() - 1, L = 0, R;
	if (!greater_than_Subs(sa[0], p, text))
		R = 0;
	else if (greater_than_Subs(sa[n], p, text))
		R = n;
	else
	{
		L = 0; R = n;
		while (R - L > 1)
		{
			M = (L + R + 1) / 2;
			if (!greater_than_Subs(sa[M], p, text))
				R = M;
			else
				L = M;
		}
	}
	return R;
}

uint32_t binSearch_R(const std::string& p, const std::vector<uint32_t>& sa, const std::string& text)
{
	std::string pp(p);
	pp.append("~");
	uint32_t M, n = sa.size() - 1, L, R;
	if (!greater_than_Subs(sa[0], pp, text))
		L = 0;
	else if (greater_than_Subs(sa[n], pp, text))
		L = n;
	else
	{
		L = 0; R = n;
		while (R - L > 1)
		{
			M = (L + R + 1) / 2;
			if (!greater_than_Subs(sa[M], pp, text))
				R = M;
			else
				L = M;
		}
	}
	return L;
}

void construct(std::vector<uint32_t>& sa, const std::string& text)
{
//	std::cout << "construct" << std::endl;
	sa.clear();
	sa.resize(text.size());
	for (size_t i = 0; i < sa.size(); i++) { sa[i] = i; }

	std::sort(sa.begin(), sa.end(), [&](int a, int b)
	{
		return less_than_Subs(a, b, text);
	});

}

void find(std::vector<uint32_t>& hits, const std::string& query, const std::vector<uint32_t>& sa, const std::string& text)
{
	hits.clear();
	if (text != "" && query != "")
	{
		//		std::cout << "find" << std::endl;

		uint32_t L = binSearch_L(query, sa, text), R = binSearch_R(query, sa, text);
		//suche in hits nach letztem treffer, wenn dieser query.length() entfernt ist, kein pushback falls is_prefix(query,
		if (R - L == 0)
		{
			if (is_prefix(sa[L], query, text))
				hits.push_back(sa[L]);
		}
		else if (R - L + 1 > 0)
		{
			for (size_t i = L; i <= R; i++)
			{
				hits.push_back(sa[i]);
			}
		}
	}
}

std::vector<uint32_t> LCP(const std::string& txt, std::vector<uint32_t>& suffixArr)
{
    uint32_t n = suffixArr.size();

    // To store LCP array
    std::vector<uint32_t> lcp(n, 0);

    // An auxiliary array to store inverse of suffix array
    // elements. For example if suffixArr[0] is 5, the
    // invSuff[5] would store 0.  This is used to get next
    // suffix string from suffix array.
    std::vector<uint32_t> invSuff(n, 0);

    // Fill values in invSuff[]
    for (size_t i=0; i < n; i++)
        invSuff[suffixArr[i]] = i;

    // Initialize length of previous LCP
    uint32_t k = 0;

    // Process all suffixes one by one starting from
    // first suffix in txt[]
    for (size_t i=0; i<n; i++)
    {
        /* If the current suffix is at n-1, then we don’t
           have next substring to consider. So lcp is not
           defined for this substring, we put zero. */
        if (invSuff[i] == n-1)
        {
            k = 0;
            continue;
        }

        /* j contains index of the next substring to
           be considered  to compare with the present
           substring, i.e., next string in suffix array */
        uint32_t j = suffixArr[invSuff[i]+1];

        // Directly start matching from k'th index as
        // at-least k-1 characters will match
        while (i+k<n && j+k<n && txt[i+k]==txt[j+k])
            k++;

        lcp[invSuff[i]] = k; // lcp for the present suffix.

        // Deleting the starting character from the string.
        if (k>0)
            k--;
    }

    // return the constructed lcp array
    return lcp;
}
