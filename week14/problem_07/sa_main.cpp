#include <iostream>
#include <string>
#include <bits/stdc++.h> 
#include "sa.hpp"
#include <vector>
#include <cstring>
#include <exception>

int main(){
    
    std::vector<uint32_t> sa;//suffix array for the normal text
    std::vector<uint32_t> sa2;//suffix array for the reverse text
    std::string text = "abaabc";
    std::string text_rev = reverse(text); //reverse the text
    construct(sa, text);//construct the suffix arrays
    construct(sa2, text_rev);
    //printc(sa);
    //printc(sa2);
    std::vector<uint32_t>lcp = LCP(text, sa); //calculate the lcps for the text (actually not necessary)
    std::vector<uint32_t>lcp2 = LCP (text_rev, sa2);
	//printc(lcp);
	//printc(lcp2);
	std::vector<uint32_t> origin; //vector storing the original place of the elements in the merged suffix array (1 for normal, 2 for reverse)
	origin.resize(2*text.length());
	std::vector<uint32_t>  merged_SA = merge(text, text_rev, sa, sa2, origin);//merge the suffix arrays
	//printc(merged_SA);
	//printc(origin);
	std::vector<uint32_t> merged_lcp = merged_LCP(text, text_rev, merged_SA, origin);//calculate the new lcp of the merged SA
	//printc(merged_lcp);
	
	
	
	//look for the longest palindrome
	std::vector<uint32_t> hits;
	int n = text.length();
	int LPL = 0; //length of longest palindrome
	std::string LP = ""; //longest palindrome
	std::vector<uint32_t> hits_LPL;
	for(uint32_t i = 0; i< n ; ++i ){
	    std::string query1 = text.substr(i, n-i);
	    std::string query2 = text_rev.substr(n - i - 1, n - (n - i - 1));
	    std::string query = longestCommonPrefix(query1, query2);
	    std::cout << "This is round: " << i << std::endl;
	    std::cout << query1 << std::endl;
	    std::cout << query2 << std::endl;
	    std::cout << query << std::endl;
	    merged_find(hits, query, merged_SA, text, text_rev, origin);
	    printc(hits);
	    if(LPL < query.length()){
	        LPL = query.length();
	        LP = query;
	        hits_LPL = hits;
	    }
	}
    
    std::cout << "The merged suffix array: ";
    printc(merged_SA);
    std::cout << "The origin vector: ";
    printc(origin);
    
    
	std::cout <<  "The palindrome substring " << LP + reverse(LP) << " at ";
	printc(hits_LPL);

    return 1;
}



