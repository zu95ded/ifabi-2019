struct suffix { 
    int index; 
    char *suff; 
}; 

int cmp(struct suffix a, struct suffix b);

int *buildSuffixArray(char *txt, int n);

void printArr(int arr[], int n);