#include <iostream> 
#include <cstring> 
#include <string>
#include <algorithm> 
#include "sa.hpp"
using namespace std; 

// Suffix Array implementation from https://www.geeksforgeeks.org/suffix-array-set-1-introduction/

int main() 
{ 
    char txt[] = "banananas"; 
    string txt_double = string(txt) + string(txt);

    int n = strlen(txt); 
    int *suffixArr = buildSuffixArray(txt,  n); 

    cout << "Minimum lexicographic rotation of '" << txt << "' is '" << txt_double.substr(suffixArr[0], n) << "'"<< endl;
    return 0; 
} 