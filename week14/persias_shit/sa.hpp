#pragma once // include guard
#include <bits/stdc++.h> 
#include <algorithm>
#include <string>
#include <vector>
#include <iostream>
#include "sa.hpp"
#include <unordered_map>

//=============================================================================
//
//=============================================================================

/// Build suffix array from text.
/// Input: the text (might be empty)
/// Output: a suffix array (sorted). The variable is cleared before its filled and returned.
void construct(std::vector<uint32_t>& sa, const std::string& text);

/// Search a string in a text via suffix array. If the query is empty, return empty hits.
/// Input: search for a 'query' string in a suffix array 'sa' build from 'text'.
/// Output: Results are returned in 'hits'. The variable is cleared before its filled and returned.
void find(std::vector<uint32_t>& hits, const std::string& query, const std::vector<uint32_t>& sa, const std::string& text);

/// kasai's algorithm from
/// https://www.geeksforgeeks.org/%C2%AD%C2%ADkasais-algorithm-for-construction-of-lcp-array-from-suffix-array/
std::vector<uint32_t> LCP(const std::string& txt, std::vector<uint32_t>& suffixArr);

//reverses a string
///Input: a string
///output: the reversed input string
std::string reverse(const std::string& str);

//merge two suffix array
//input: the text, reversed text, suffix Arrays, suffix array of the reversed text, origin vecotr
std::vector<uint32_t> merge(const std::string& txt, const std::string& rev_txt, std::vector<uint32_t>& suffixArr, std::vector<uint32_t>& rev_suffixArr, std::vector<uint32_t>& origin);


//compute the LCP of the merged SA
//input: Text, reversed text, the merged suffix array, origin vector
//output: LCP of merged Suffix array
std::vector<uint32_t> merged_LCP(const std::string& txt, const std::string& rev_txt, std::vector<uint32_t>& suffixArr, std::vector<uint32_t>& origin);




/// helper function to print a container
template <typename T>
void printc(const T& v)
{
  std::cout << *v.begin();
	for (auto it = v.begin() + 1; it != v.end(); it++)
    std::cout << ' ' << *it;
  std::cout << '\n';
}

bool less_than_Subs(uint32_t a, uint32_t b, const std::string& text)
//ist pr�fix text[a] kleiner pr�fix text[b]? Hilfsfunktion f�r construct()
{
	if (text.length() > 0 && a < text.length() && b < text.length())
	{
		while (true)
		{
			if (text[a] < text[b])
				return true;
			else if (text[a] > text[b])
				return false;
			else
			{
				//if ((a + 1) >= text.length() && (b + 1) >= text.length())
				//	return false;
				if ((a + 1) >= text.length())
					return true;
				else if ((b + 1) >= text.length())
					return false;
				else
				{
					++a; ++b;
				}
			}
		}
	}
	else
		return false;
}

bool less_than_Subs(uint32_t a, const std::string& p, const std::string& text)
//ist p kleiner pr�fix von text[a]? Hilfsfunktion f�r binSearch
{
	if (p == "")
		return false;
	if (p.length() > 0 && text.length() > 0 && a < text.length())
	{
		uint32_t b = 0;
		while (true)
		{
			if (p[b] < text[a])
				return true;
			else if (p[b] > text[a])
				return false;
			else // wenn p[b] == text[a]
			{
				if ((a + 1) >= text.length() && (b + 1) >= p.length())
					return false;
				else if ((a + 1) >= text.length())
					return false;
				else if ((b + 1) >= p.length())
					return true;
				else
				{
					++a; ++b;
				}
			}
		}
	}
	else
		return false;
}

bool greater_than_Subs(uint32_t a, const std::string& p, const std::string& text)
//ist p groesser suffix(text[a]) Hilfsfunktion f�r binSearch
{
	if (p == "")
		return false;
	if (p.length() > 0 && text.length() > 0 && a < text.length())
	{
		uint32_t b = 0;
		while (true)
		{
			if (p[b] > text[a]) // p = Q, SA[M] = text[a]
				return true;
			else if (p[b] < text[a])
				return false;
			else
			{
				if ((a + 1) >= text.length() && (b + 1) >= p.length())
					return false;
				else if ((a + 1) >= text.length())
					return true;
				else if ((b + 1) >= p.length())
					return false;
				else
				{
					++a; ++b;
				}
			}
		}
	}
	else
		return false;
}

bool is_prefix(uint32_t a, const std::string& p, const std::string& text)
{
	if (text.length() - a < p.length())
		return false;
	for (size_t i = 0; i < p.size(); i++)
	{
		if (p[i] == text[a])
		{
			++a;
		}
		else
			return false;
	}
	return true;
}

uint32_t binSearch_L(const std::string& p, const std::vector<uint32_t>& sa, const std::string& text)
{
	uint32_t M, n = sa.size() - 1, L = 0, R;
	if (!greater_than_Subs(sa[0], p, text))
		R = 0;
	else if (greater_than_Subs(sa[n], p, text))
		R = n;
	else
	{
		L = 0; R = n;
		while (R - L > 1)
		{
			M = (L + R + 1) / 2;
			if (!greater_than_Subs(sa[M], p, text))
				R = M;
			else
				L = M;
		}
	}
	return R;
}

uint32_t binSearch_R(const std::string& p, const std::vector<uint32_t>& sa, const std::string& text)
{
	std::string pp(p);
	pp.append("~");
	uint32_t M, n = sa.size() - 1, L, R;
	if (!greater_than_Subs(sa[0], pp, text))
		L = 0;
	else if (greater_than_Subs(sa[n], pp, text))
		L = n;
	else
	{
		L = 0; R = n;
		while (R - L > 1)
		{
			M = (L + R + 1) / 2;
			if (!greater_than_Subs(sa[M], pp, text))
				R = M;
			else
				L = M;
		}
	}
	return L;
}

void construct(std::vector<uint32_t>& sa, const std::string& text)
{
//	std::cout << "construct" << std::endl;
	sa.clear();
	sa.resize(text.size());
	for (size_t i = 0; i < sa.size(); i++) { sa[i] = i; }

	std::sort(sa.begin(), sa.end(), [&](int a, int b)
	{
		return less_than_Subs(a, b, text);
	});

}

void find(std::vector<uint32_t>& hits, const std::string& query, const std::vector<uint32_t>& sa, const std::string& text)
{
	hits.clear();
	if (text != "" && query != "")
	{
		//		std::cout << "find" << std::endl;

		uint32_t L = binSearch_L(query, sa, text), R = binSearch_R(query, sa, text);
		//suche in hits nach letztem treffer, wenn dieser query.length() entfernt ist, kein pushback falls is_prefix(query,
		if (R - L == 0)
		{
			if (is_prefix(sa[L], query, text))
				hits.push_back(sa[L]);
		}
		else if (R - L + 1 > 0)
		{
			for (size_t i = L; i <= R; i++)
			{
				hits.push_back(sa[i]);
			}
		}
	}
}

std::vector<uint32_t> LCP(const std::string& txt, std::vector<uint32_t>& suffixArr)
{
    uint32_t n = suffixArr.size();

    // To store LCP array
    std::vector<uint32_t> lcp(n, 0);

    // An auxiliary array to store inverse of suffix array
    // elements. For example if suffixArr[0] is 5, the
    // invSuff[5] would store 0.  This is used to get next
    // suffix string from suffix array.
    std::vector<uint32_t> invSuff(n, 0);

    // Fill values in invSuff[]
    for (size_t i=0; i < n; i++)
        invSuff[suffixArr[i]] = i;

    // Initialize length of previous LCP
    uint32_t k = 0;

    // Process all suffixes one by one starting from
    // first suffix in txt[]
    for (size_t i=0; i<n; i++)
    {
        /* If the current suffix is at n-1, then we don’t
           have next substring to consider. So lcp is not
           defined for this substring, we put zero. */
        if (invSuff[i] == n-1)
        {
            k = 0;
            continue;
        }

        /* j contains index of the next substring to
           be considered  to compare with the present
           substring, i.e., next string in suffix array */
        uint32_t j = suffixArr[invSuff[i]+1];

        // Directly start matching from k'th index as
        // at-least k-1 characters will match
        while (i+k<n && j+k<n && txt[i+k]==txt[j+k])
            k++;

        lcp[invSuff[i]] = k; // lcp for the present suffix.

        // Deleting the starting character from the string.
        if (k>0)
            k--;
    }

    // return the constructed lcp array
    return lcp;
}


std::string reverse(const std::string& str){
    int n = str.length(); 
    std::string rev = str;
    // Swap character starting from two 
    // corners 
    for (int i = 0; i < n / 2; i++) {
        char swap = rev[i];
        rev[i] = rev[n - i - 1];
        rev[n - i - 1] = swap;
    }
    return rev;
}

std::vector<uint32_t> merge(const std::string& txt, const std::string& rev_txt, std::vector<uint32_t>& suffixArr, std::vector<uint32_t>& rev_suffixArr, std::vector<uint32_t>& origin){
    std::vector<uint32_t> merged_SA;
    int n = suffixArr.size();
    merged_SA.resize(2*n);
    
    uint32_t last_merged = 0;
    uint32_t index = 0;
    for(uint32_t i = 0; i < n; ++i){
        uint32_t current = suffixArr[i];
        uint32_t current_last_merged = last_merged;
        
        if(current_last_merged == n){//every element of the reverse SA is already in the new SA
            merged_SA[index] = current;
                origin[index] = 1;
                ++ index;
        }
        
        for(uint32_t j = current_last_merged; j < n; ++j){
            uint32_t rev_current = rev_suffixArr[j];
            std::string sub1 = txt.substr(current, n - current);
            std::string sub2 = rev_txt.substr(rev_current, n - rev_current);
            
            if(sub1 <= sub2){
                merged_SA[index] = current;
                origin[index] = 1;
                ++ index;
                break;
            } else {
                merged_SA[index] = rev_current;
                origin[index] = 2;
                ++index; 
                ++last_merged;
            }
        }
    }
    if(last_merged < n){
        for(uint32_t i = last_merged; i < n; ++i){
            merged_SA[index] = rev_suffixArr[i];
            origin[index] = 2;
            ++index; 
            ++last_merged;
        }
    }
    
    return merged_SA;
}


std::vector<uint32_t> merged_LCP(const std::string& txt, const std::string& rev_txt, std::vector<uint32_t>& suffixArr, std::vector<uint32_t>& origin)
{
    uint32_t m = suffixArr.size();
    uint32_t n = txt.length();
    // To store LCP array
    std::vector<uint32_t> lcp(m, 0);

    // An auxiliary array to store inverse of suffix array
    // elements. For example if suffixArr[0] is 5, the
    // invSuff[5] would store 0.  This is used to get next
    // suffix string from suffix array.
    
    // Fill values in invSuff[]
    // Initialize length of previous LCP

    // Process all suffixes one by one starting from
    // first suffix in txt[]
    for (size_t index=0; index<m - 1; index++)
    {
        uint32_t k = 0;
        uint32_t i = suffixArr[index];
        uint32_t j = suffixArr[index + 1];
        // Directly start matching from k'th index as
        // at-least k-1 characters will match
        if(origin[index] == 1 && origin[index+1] == 1){
            while (i+k<n && j+k<n && txt[i+k]==txt[j+k]){
                k++;
            }
        }else if(origin[index] == 2 && origin[index + 1] == 2){
            while (i+k<n && j+k<n && rev_txt[i+k]==rev_txt[j+k]){
                k++;
            }
        }else if(origin[index] == 1 && origin[index + 1] == 2){
            while (i+k<n && j+k<n && txt[i+k]==rev_txt[j+k]){
                k++;
            }
        } else {
            while (i+k<n && j+k<n && rev_txt[i+k]==txt[j+k]){
                k++;
            }
        }
        
        lcp[index] = k; // lcp for the present suffix.

        // Deleting the starting character from the string.
        if (k>0)
            k--;
    }

    // return the constructed lcp array
    return lcp;
}




