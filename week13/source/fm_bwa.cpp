
#include <seqan3/io/sequence_file/all.hpp>
#include <seqan3/search/algorithm/search.hpp>
#include <seqan3/alphabet/nucleotide/dna5.hpp>
#include <array>
#include <ctime>
#include <cmath>
#include <seqan3/core/debug_stream.hpp>
#include <bits/stdc++.h>
#include <chrono>

using namespace seqan3;

// ----------------------------------------------------------------------------
// iterator to generate every k-mer of a dn5 string
// ----------------------------------------------------------------------------

template <int K>
class kmer_iter
{
public:
  kmer_iter(std::vector<dna5> & text): text_iter(text.begin()) {}
  ~kmer_iter(){}
  std::array<dna5, K> roll()
  {
    std::array<dna5, K> kmer;
    for (size_t i{0}; i < K; ++i)
      kmer[i]= *(text_iter+offset+i);
    ++offset;
    return kmer;
  }
private:
  std::vector<dna5>::iterator text_iter;
  size_t offset{0};
};

// ----------------------------------------------------------------------------
// similarity based on bwa fm index search
// ----------------------------------------------------------------------------

template <int K>
double similarity(std::vector<dna5> & s0, std::vector<dna5> & s1)
{
  time_t start_time = time(NULL);
  seqan3::fm_index index{s1};
  //debug_stream << (time(NULL) - start_time) << " sec. to build index of " << s0.size() << " long dna5\n";

  start_time = time(NULL);
  size_t hits{0};
  kmer_iter<K> KI(s0);
  for (size_t i(0); i < s0.size()-K; ++i)
    hits += (search(KI.roll(), index).size() > 0);
  //debug_stream << (time(NULL) - start_time) << " sec. to search for " << (s1.size()-T) << " k-mers of size " << T << "\n";

  return (double)(hits) / (double)(s0.size()-10);
};


// ----------------------------------------------------------------------------
// Bloomfilter
// ----------------------------------------------------------------------------
// mit zwei hash-functions,
// p=0.001,
// |alphabet| = 5
// is:
//-(((5**10/2) * math.log(0.001)/(math.log(2)**2)))

// K = size k-mer, U = size bitset, H = number of hash functions
template <size_t K, size_t U, size_t H>
class Bloomfilter
{
public:
  Bloomfilter(){};
  ~Bloomfilter(){};

  void add(std::array<dna5, K> arr)
  {
    std::array<size_t, H> hashes = hash(arr);
    for (size_t i(0); i < H; ++i)
      B[hashes[i]] = 1;
  }

  bool exist(std::array<dna5, K> arr)
  {
    auto hashes = hash(arr);
    size_t hit(0);
    for (size_t i(0); i < H; ++i)
      hit+=B[hashes[i]];
    if (hit == H)
      return true;
    else
      return false;
  }

  void print() { debug_stream << B << "\n"; }

private:
  std::bitset<U> B;

  std::array<size_t, H> hash(std::array<dna5, K> arr)
  {
    std::array<size_t, H> hash_values{0};
    for (size_t i(0); i < K; ++i)
    {
      hash_values[i%H] *= (size_t)(std::pow(5,((i/H))));
      hash_values[i%H] += to_rank(arr[i]);
    }
    for (size_t i(0); i < H; ++i){
      hash_values[i] = hash_values[i]%U;
    }
    return hash_values;
  }
};

// ----------------------------------------------------------------------------
// similarity based n bloom filter.
// ----------------------------------------------------------------------------

template <int K, int U, int H>
double bloomality(std::vector<dna5> & s0, std::vector<dna5> & s1)
{
  // [ ! ] creates segfault with very large U. -> moving to heap could solve it
  // https://www.gamedev.net/forums/topic/569935-maximum-size-of-a-bitset/
  // just make B in Bloomfilter "new" B or maybe here:
  // Bloomfilter<K, U, H> new BF; -> but it's slow as shit.
  Bloomfilter<K, U, H> BF;

  kmer_iter<K> K0(s0);
  for (size_t i(0); i < s0.size()-K; ++i)
    BF.add(K0.roll());

  size_t c(0);
  kmer_iter<K> K1(s1);
  for (size_t i(0); i < s1.size()-K; ++i)
    c += BF.exist(K1.roll());

  return ((double)c / (double)(s1.size()-K));
};


// ----------------------------------------------------------------------------
// main
// ----------------------------------------------------------------------------

int main()
{
  auto t1 = std::chrono::high_resolution_clock::now();
  // --------------------------------------------------------------------------
  // data loading
  time_t start_time = time(NULL);

  //sequence_file_input Xmouse{"../genomes/mm_ref_GRCm38.p6_chrX.head.fa"};
  sequence_file_input Xfelis{"../genomes/fca_ref_Felis_catus_9.0_chrX.10k.fa"};
  sequence_file_input Xlynx{"../genomes/61383_ref_mLynCan4_v1.p_chrX.10k.fa"};

  std::vector<dna5> dnaFelis(get<seqan3::field::seq>(*Xfelis.begin()));
  std::vector<dna5> dnaLynx( get<seqan3::field::seq>( *Xlynx.begin()));
  //std::vector<dna5> dnaMouse(get<seqan3::field::seq>(*Xmouse.begin()));

  debug_stream << (time(NULL) - start_time) << " sec. -- genomes loaded to memory.\n";

  // --------------------------------------------------------------------------
  // Bloom filtering
  /*
  const size_t K(15); // K-mer size
  const size_t H(2); // number of hash functions
  const size_t U(203063); // size of Bitvector in Bloomfilter (check for segfault if much bigger)
  start_time = time(NULL);
  // 2 hash-functions
  double c = bloomality<K,U,H>(dnaFelis,dnaLynx);
  debug_stream << (time(NULL) - start_time) << " sec. -- bloom filter ran.\n";

  debug_stream << c << " ratio felis to lynx\n";
  */

// ----------------------------------------------------------------------------
// bwa k-mer counting
  //debug_stream << "cat to lynx has "   << similarity<10>(dnaFelis, dnaLynx)  << " common 10-mer ratio\n";
  //debug_stream << "cat to mouse has "  << similarity<10>(dnaFelis, dnaMouse) << " common 10-mer ratio\n";
  //debug_stream << "mouse to lynx has " << similarity<10>(dnaMouse, dnaLynx)  << " common 10-mer ratio\n";

  debug_stream << "cat to lynx has "   << similarity<15>(dnaFelis, dnaLynx)  << " common 15-mer ratio\n";
  //debug_stream << "cat to mouse has "  << similarity<15>(dnaFelis, dnaMouse) << " common 15-mer ratio\n";
  //debug_stream << "mouse to lynx has " << similarity<15>(dnaMouse, dnaLynx)  << " common 15-mer ratio\n";


  // ----------------------------------------------------------------------------
  auto t2 = std::chrono::high_resolution_clock::now();
  auto duration = std::chrono::duration_cast<std::chrono::seconds>( t2 - t1 ).count();
  std::cout << "Duration: " << duration << "ms" << std::endl;
  seqan3::debug_stream << "\nDONE\n";

}
