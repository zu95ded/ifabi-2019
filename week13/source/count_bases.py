import sys

infile = sys.argv[1]

f = open(infile)
num_bases = 0
for line in f:
    if not line.startswith('>'):
        num_bases += len(line)

print(num_bases)