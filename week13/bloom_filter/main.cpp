/**********https://www.onlinegdb.com/online_c++_compiler#_editor_3991599981********************************************************************

                              Online C++ Compiler.
               Code, Compile, Run and Debug C++ program online.
Write your code in this editor and press "Run" button to compile and execute it.

*******************************************************************************/
#include <iostream>
#include <string>
#include "BloomFilter.h"
#include <math.h>
#include <functional>
#include <chrono>
using namespace std;

int main (int argc, char* argv[])
{   
    auto t1 = chrono::high_resolution_clock::now();

    // define a bloom filter object: 2 parameters ==> 1. size of bit map, 2. number of hash functions
    string corpus_file = argv[1];
    string corpus_string = readFasta(corpus_file);

    int k_kmeresize = 15;
    int n_numelements = corpus_string.length() - k_kmeresize;
    float p_falsepositiverate = 0.00001;
    int m_bitsize = -((n_numelements * log(p_falsepositiverate)) / pow(log(2), 2));
    int h_numhash = (m_bitsize / n_numelements) * log(2);
    cout << "For p = " << p_falsepositiverate << ", m = " << m_bitsize << endl;

    BloomFilter BM = BloomFilter(m_bitsize, h_numhash);

    //first add all kmeres with BM.add(kmere)
    BM.createFilter(corpus_string, k_kmeresize);

    //then you can use BM.possiblycontains(kmere to find the kmeres in the)
    string pattern_file = argv[2];
    string pattern_string = readFasta(pattern_file);
    int num_in = BM.compareGenomes(pattern_string, k_kmeresize);
    cout << "Sequence identity: " << static_cast<double>(num_in) / pattern_string.length() * 100 << "%" << endl;
    auto t2 = chrono::high_resolution_clock::now();
    auto duration = chrono::duration_cast<chrono::milliseconds>( t2 - t1 ).count();
    std::cout << "Duration: " << duration << "ms" << endl;

    return 0;
};
