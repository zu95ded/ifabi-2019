#include "BloomFilter.h"
#include <string>
#include <math.h>
#include <fstream>
#include <iostream>
#include <functional>


BloomFilter::BloomFilter(uint64_t size, uint8_t numHashes)
      : m_bits(size),
        m_numHashes(numHashes) {}


uint64_t BloomFilter::hash1(std::string kmere) const{
    uint64_t hash_value = 0;
    for (int i = 0; i < kmere.length(); i++) {
      hash_value = hash_value + BaseToNum(kmere[i]) * pow(2, i);
    }
    //std::hash<std::string> str_hash;
    //uint64_t hash_value = str_hash(kmere);
    return hash_value % 7;
}


uint64_t BloomFilter::hash2(std::string kmere) const{
    //uint64_t hash_value = 0;
    //int end = kmere.size() - 1;
    //hash_value = BaseToNum(kmere[end])*1 + BaseToNum(kmere[end - 1])*2 + BaseToNum(kmere[end - 2])*4 + BaseToNum(kmere[end - 3])*8 + BaseToNum(kmere[end - 4])*16;
    std::hash<std::string> str_hash;
    uint64_t hash_value = str_hash(kmere);
    return hash_value;
}


void BloomFilter::add(std::string kmere) {
  uint64_t hashA = hash1(kmere);
  uint64_t hashB = hash2(kmere);

  for (int n = 0; n < m_numHashes; n++) {
    uint64_t pos = (hashA + n * hashB) % m_bits.size();
    m_bits[pos] = true;
  }
}


void BloomFilter::createFilter(std::string corpus_string, int k){
  for (int i = 0; i <= corpus_string.length() - k; i++) {
    add(corpus_string.substr(i, k));
  }
}


int BloomFilter::possiblyContains(std::string kmere) const{//, std::size_t len) const {
  //auto hashValues = hash(data, len);
    uint64_t hashA = hash1(kmere);
    uint64_t hashB = hash2(kmere);

  for (int n = 0; n < m_numHashes; n++) {
      uint64_t pos = (hashA + n * hashB) % m_bits.size();
      if (!m_bits[pos]) {
          return 0;
      }
  }
  return 1;
}


int BloomFilter::compareGenomes(std::string pattern_string, int k) const {
  int num_in = 0;
  for (int i = 0; i <= pattern_string.length() - k; i++) {
    num_in = num_in + possiblyContains(pattern_string.substr(i, k));
  }
  return num_in;
}


int BloomFilter::BaseToNum(char base) const{
    if(base == 'A' or base == 'a'){
        return 0;
    } else if(base == 'C' or base == 'c'){
        return 1;
    } else if(base == 'G' or base == 'g'){
        return 2;
    } else if(base == 'T' or base == 't'){
        return 3;
    } else {
        return 4; // case N for dna5
    }
}


string readFasta(string const in_file) {
  std::ifstream data (in_file);
  std::string str;
  std::string seq;

  if (data.good()){
    while(getline(data, str)){ 
      if (str[0] != '>'){
        seq = seq + str; 
      }
    }
    return seq;
  }
  else {
    return "";
  }
}
