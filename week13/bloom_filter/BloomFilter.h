#include <vector>
#include <string>

using namespace std;

struct BloomFilter {
  BloomFilter(uint64_t size, uint8_t numHashes);

  void add(std::string kmere);//, std::size_t len);
  void createFilter(std::string corpus_string, int k);
  int compareGenomes(std::string pattern_string, int k) const;
  int possiblyContains(std::string kmere) const; // std::size_t len) const;
  uint64_t hash1(std::string kmere) const;
  uint64_t hash2(std::string kmere) const;
  int BaseToNum(char base) const;

private:
  uint8_t m_numHashes;
  std::vector<bool> m_bits;
};

string readFasta(string const in_file);
