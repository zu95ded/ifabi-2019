#!/usr/bin/env python
# coding: utf-8


from PIL import Image
import numpy as np
import os
import glob


#parameters
target_resolution = (250,250)

# load file paths
# contains all malificant images
mal_in_dir = "/media/memsonmi/STICK/breast/COLLECTED/mal/"
# contains all benign images
ben_in_dir = "/media/memsonmi/STICK/breast/COLLECTED/ben/"

# will contain all resized malificant images
mal_out_dir = "/media/memsonmi/STICK/breast/COLLECTED_resized/mal/"
# will contain all resized benign images
ben_out_dir = "/media/memsonmi/STICK/breast/COLLECTED_resized/ben/"


# file detection in givel directories
mal_in_paths = glob.glob(mal_in_dir+"*.png")
ben_in_paths = glob.glob(ben_in_dir+"*.png")


# resize and save benign files
for i,s in enumerate(ben_in_paths):
    Image.open(s).resize(target_resolution).save(ben_out_dir+os.path.basename(ben_in_paths[i]))

# resize and save malignant files
for i,s in enumerate(mal_in_paths):
    Image.open(s).resize(target_resolution).save(mal_out_dir+os.path.basename(mal_in_paths[i]))

