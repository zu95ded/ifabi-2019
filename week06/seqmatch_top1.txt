Seqmatch:version 3
RDP Data:release11_5
Data Set:both type and non-type strains, 
:both environmental (uncultured) sequences and isolates, 
:near-full-length sequences (>=1200 bases), 
:good quality sequences
Comments:1558793 sequences were included in the search. The screening was based on 7-base oligomers.
Query Submit Date:Sat Nov 30 15:11:57 EST 2019
Note:Orientation "-" means the query sequence has been reverse-complemented when the match is performed

query name	short ID	orientation	similarity score	S_ab score	unique common oligomers	sequence name	taxon rank	taxon name
seqmatch_seq	S000021994	+	not_calculated	1.000	1297	Eggerthella sp. MLG043; AF304434	rootrank	Root	domain	Bacteria	phylum	"Actinobacteria"	class	Actinobacteria	subclass	Coriobacteridae	order	Coriobacteriales	suborder	"Coriobacterineae"	family	Coriobacteriaceae	genus	Eggerthella
seqmatch_seq	S000382128	+	not_calculated	1.000	1353	Eggerthella lenta; JCM9979; AB011817	rootrank	Root	domain	Bacteria	phylum	"Actinobacteria"	class	Actinobacteria	subclass	Coriobacteridae	order	Coriobacteriales	suborder	"Coriobacterineae"	family	Coriobacteriaceae	genus	Eggerthella
seqmatch_seq	S000390779	+	not_calculated	1.000	1384	Eggerthella lenta (T); ATCC25559; AF292375	rootrank	Root	domain	Bacteria	phylum	"Actinobacteria"	class	Actinobacteria	subclass	Coriobacteridae	order	Coriobacteriales	suborder	"Coriobacterineae"	family	Coriobacteriaceae	genus	Eggerthella
seqmatch_seq	S000824766	+	not_calculated	1.000	1222	Eggerthella sp. SDG-2; EF413638	rootrank	Root	domain	Bacteria	phylum	"Actinobacteria"	class	Actinobacteria	subclass	Coriobacteridae	order	Coriobacteriales	suborder	"Coriobacterineae"	family	Coriobacteriaceae	genus	Eggerthella
seqmatch_seq	S001115913	+	not_calculated	1.000	1237	uncultured bacterium; A1_737; EU761994	rootrank	Root	domain	Bacteria	phylum	"Actinobacteria"	class	Actinobacteria	subclass	Coriobacteridae	order	Coriobacteriales	suborder	"Coriobacterineae"	family	Coriobacteriaceae	genus	Eggerthella
seqmatch_seq	S001217855	+	not_calculated	1.000	1268	uncultured bacterium; TS30_a01b03; FJ367918	rootrank	Root	domain	Bacteria	phylum	"Actinobacteria"	class	Actinobacteria	subclass	Coriobacteridae	order	Coriobacteriales	suborder	"Coriobacterineae"	family	Coriobacteriaceae	genus	Eggerthella
seqmatch_seq	S002165149	+	not_calculated	1.000	1360	Eggerthella lenta; JCM 9979; AB558167	rootrank	Root	domain	Bacteria	phylum	"Actinobacteria"	class	Actinobacteria	subclass	Coriobacteridae	order	Coriobacteriales	suborder	"Coriobacterineae"	family	Coriobacteriaceae	genus	Eggerthella
seqmatch_seq	S002166843	+	not_calculated	1.000	1337	Eggerthella lenta; 34; GU968179	rootrank	Root	domain	Bacteria	phylum	"Actinobacteria"	class	Actinobacteria	subclass	Coriobacteridae	order	Coriobacteriales	suborder	"Coriobacterineae"	family	Coriobacteriaceae	genus	Eggerthella
seqmatch_seq	S002261338	+	not_calculated	1.000	1236	uncultured bacterium; nby479d07c1; HM828709	rootrank	Root	domain	Bacteria	phylum	"Actinobacteria"	class	Actinobacteria	subclass	Coriobacteridae	order	Coriobacteriales	suborder	"Coriobacterineae"	family	Coriobacteriaceae	genus	Eggerthella
seqmatch_seq	S002289680	+	not_calculated	1.000	1392	Eggerthella lenta DSM 2243; CP001726	rootrank	Root	domain	Bacteria	phylum	"Actinobacteria"	class	Actinobacteria	subclass	Coriobacteridae	order	Coriobacteriales	suborder	"Coriobacterineae"	family	Coriobacteriaceae	genus	Eggerthella
seqmatch_seq	S002310527	+	not_calculated	1.000	1269	uncultured bacterium; 16slp49-02a08.p1k; FJ504348	rootrank	Root	domain	Bacteria	phylum	"Actinobacteria"	class	Actinobacteria	subclass	Coriobacteridae	order	Coriobacteriales	suborder	"Coriobacterineae"	family	Coriobacteriaceae	genus	Eggerthella
seqmatch_seq	S002310543	+	not_calculated	1.000	1260	uncultured bacterium; 16slp49-04h11.p1k; FJ504364	rootrank	Root	domain	Bacteria	phylum	"Actinobacteria"	class	Actinobacteria	subclass	Coriobacteridae	order	Coriobacteriales	suborder	"Coriobacterineae"	family	Coriobacteriaceae	genus	Eggerthella
seqmatch_seq	S002310861	+	not_calculated	1.000	1260	uncultured bacterium; 16slp50-04d03.p1k; FJ504682	rootrank	Root	domain	Bacteria	phylum	"Actinobacteria"	class	Actinobacteria	subclass	Coriobacteridae	order	Coriobacteriales	suborder	"Coriobacterineae"	family	Coriobacteriaceae	genus	Eggerthella
seqmatch_seq	S002310887	+	not_calculated	1.000	1258	uncultured bacterium; 16slp50-04a06.p1k; FJ504708	rootrank	Root	domain	Bacteria	phylum	"Actinobacteria"	class	Actinobacteria	subclass	Coriobacteridae	order	Coriobacteriales	suborder	"Coriobacterineae"	family	Coriobacteriaceae	genus	Eggerthella
seqmatch_seq	S002310904	+	not_calculated	1.000	1258	uncultured bacterium; 16slp50-04a09.p1k; FJ504725	rootrank	Root	domain	Bacteria	phylum	"Actinobacteria"	class	Actinobacteria	subclass	Coriobacteridae	order	Coriobacteriales	suborder	"Coriobacterineae"	family	Coriobacteriaceae	genus	Eggerthella
seqmatch_seq	S002310954	+	not_calculated	1.000	1257	uncultured bacterium; 16slp50-06h02.p1k; FJ504775	rootrank	Root	domain	Bacteria	phylum	"Actinobacteria"	class	Actinobacteria	subclass	Coriobacteridae	order	Coriobacteriales	suborder	"Coriobacterineae"	family	Coriobacteriaceae	genus	Eggerthella
seqmatch_seq	S002311759	+	not_calculated	1.000	1265	uncultured bacterium; 16slp52-04b01.p1ka; FJ505580	rootrank	Root	domain	Bacteria	phylum	"Actinobacteria"	class	Actinobacteria	subclass	Coriobacteridae	order	Coriobacteriales	suborder	"Coriobacterineae"	family	Coriobacteriaceae	genus	Eggerthella
seqmatch_seq	S002312179	+	not_calculated	1.000	1259	uncultured bacterium; 16slp47-03a09.p1k; FJ506000	rootrank	Root	domain	Bacteria	phylum	"Actinobacteria"	class	Actinobacteria	subclass	Coriobacteridae	order	Coriobacteriales	suborder	"Coriobacterineae"	family	Coriobacteriaceae	genus	Eggerthella
seqmatch_seq	S002312408	+	not_calculated	1.000	1268	uncultured bacterium; 16slp53-03c04.p1k; FJ506229	rootrank	Root	domain	Bacteria	phylum	"Actinobacteria"	class	Actinobacteria	subclass	Coriobacteridae	order	Coriobacteriales	suborder	"Coriobacterineae"	family	Coriobacteriaceae	genus	Eggerthella
seqmatch_seq	S002318030	+	not_calculated	1.000	1265	uncultured bacterium; 16slp85-11g11.p1k; FJ511851	rootrank	Root	domain	Bacteria	phylum	"Actinobacteria"	class	Actinobacteria	subclass	Coriobacteridae	order	Coriobacteriales	suborder	"Coriobacterineae"	family	Coriobacteriaceae	genus	Eggerthella


