Seqmatch:version 3
RDP Data:release11_5
Data Set:both type and non-type strains, 
:both environmental (uncultured) sequences and isolates, 
:near-full-length sequences (>=1200 bases), 
:good quality sequences
Comments:1558793 sequences were included in the search. The screening was based on 7-base oligomers.
Query Submit Date:Sat Nov 30 15:29:49 EST 2019
Note:Orientation "-" means the query sequence has been reverse-complemented when the match is performed

query name	short ID	orientation	similarity score	S_ab score	unique common oligomers	sequence name	taxon rank	taxon name
seqmatch_seq	S000519333	+	not_calculated	1.000	1289	uncultured bacterium; LD95; AY977733	rootrank	Root	domain	Bacteria	phylum	Firmicutes	class	Clostridia	order	Clostridiales	family	Ruminococcaceae	genus	Oscillibacter
seqmatch_seq	S000521264	+	not_calculated	1.000	1286	uncultured bacterium; M379; AY979664	rootrank	Root	domain	Bacteria	phylum	Firmicutes	class	Clostridia	order	Clostridiales	family	Ruminococcaceae	genus	Oscillibacter
seqmatch_seq	S000521291	+	not_calculated	1.000	1290	uncultured bacterium; M419; AY979691	rootrank	Root	domain	Bacteria	phylum	Firmicutes	class	Clostridia	order	Clostridiales	family	Ruminococcaceae	genus	Oscillibacter
seqmatch_seq	S000521345	+	not_calculated	1.000	1288	uncultured bacterium; M492; AY979745	rootrank	Root	domain	Bacteria	phylum	Firmicutes	class	Clostridia	order	Clostridiales	family	Ruminococcaceae	genus	Oscillibacter
seqmatch_seq	S000521916	+	not_calculated	1.000	1287	uncultured bacterium; MY81; AY980316	rootrank	Root	domain	Bacteria	phylum	Firmicutes	class	Clostridia	order	Clostridiales	family	Ruminococcaceae	genus	Oscillibacter
seqmatch_seq	S000525386	+	not_calculated	1.000	1287	uncultured bacterium; M821; AY983786	rootrank	Root	domain	Bacteria	phylum	Firmicutes	class	Clostridia	order	Clostridiales	family	Ruminococcaceae	genus	Oscillibacter
seqmatch_seq	S000525387	+	not_calculated	1.000	1288	uncultured bacterium; M822; AY983787	rootrank	Root	domain	Bacteria	phylum	Firmicutes	class	Clostridia	order	Clostridiales	family	Ruminococcaceae	genus	Oscillibacter
seqmatch_seq	S000525422	+	not_calculated	1.000	1277	uncultured bacterium; M902; AY983822	rootrank	Root	domain	Bacteria	phylum	Firmicutes	class	Clostridia	order	Clostridiales	family	Ruminococcaceae	genus	Oscillibacter
seqmatch_seq	S000525465	+	not_calculated	1.000	1285	uncultured bacterium; M991; AY983865	rootrank	Root	domain	Bacteria	phylum	Firmicutes	class	Clostridia	order	Clostridiales	family	Ruminococcaceae	genus	Oscillibacter
seqmatch_seq	S000525578	+	not_calculated	1.000	1287	uncultured bacterium; MC71; AY983978	rootrank	Root	domain	Bacteria	phylum	Firmicutes	class	Clostridia	order	Clostridiales	family	Ruminococcaceae	genus	Oscillibacter
seqmatch_seq	S000525579	+	not_calculated	1.000	1286	uncultured bacterium; MC75; AY983979	rootrank	Root	domain	Bacteria	phylum	Firmicutes	class	Clostridia	order	Clostridiales	family	Ruminococcaceae	genus	Oscillibacter
seqmatch_seq	S000526789	+	not_calculated	1.000	1283	uncultured bacterium; C033; AY985189	rootrank	Root	domain	Bacteria	phylum	Firmicutes	class	Clostridia	order	Clostridiales	family	Ruminococcaceae	genus	Oscillibacter
seqmatch_seq	S000526812	+	not_calculated	1.000	1285	uncultured bacterium; C076; AY985212	rootrank	Root	domain	Bacteria	phylum	Firmicutes	class	Clostridia	order	Clostridiales	family	Ruminococcaceae	genus	Oscillibacter
seqmatch_seq	S000526818	+	not_calculated	1.000	1284	uncultured bacterium; C082; AY985218	rootrank	Root	domain	Bacteria	phylum	Firmicutes	class	Clostridia	order	Clostridiales	family	Ruminococcaceae	genus	Oscillibacter
seqmatch_seq	S000526825	+	not_calculated	1.000	1280	uncultured bacterium; C091; AY985225	rootrank	Root	domain	Bacteria	phylum	Firmicutes	class	Clostridia	order	Clostridiales	family	Ruminococcaceae	genus	Oscillibacter
seqmatch_seq	S000526886	+	not_calculated	1.000	1290	uncultured bacterium; C179; AY985286	rootrank	Root	domain	Bacteria	phylum	Firmicutes	class	Clostridia	order	Clostridiales	family	Ruminococcaceae	genus	Oscillibacter
seqmatch_seq	S000526939	+	not_calculated	1.000	1286	uncultured bacterium; C252; AY985339	rootrank	Root	domain	Bacteria	phylum	Firmicutes	class	Clostridia	order	Clostridiales	family	Ruminococcaceae	genus	Oscillibacter
seqmatch_seq	S000527018	+	not_calculated	1.000	1286	uncultured bacterium; C362; AY985418	rootrank	Root	domain	Bacteria	phylum	Firmicutes	class	Clostridia	order	Clostridiales	family	Ruminococcaceae	genus	Oscillibacter
seqmatch_seq	S000527030	+	not_calculated	1.000	1292	uncultured bacterium; C375; AY985430	rootrank	Root	domain	Bacteria	phylum	Firmicutes	class	Clostridia	order	Clostridiales	family	Ruminococcaceae	genus	Oscillibacter
seqmatch_seq	S000527066	+	not_calculated	1.000	1288	uncultured bacterium; C422; AY985466	rootrank	Root	domain	Bacteria	phylum	Firmicutes	class	Clostridia	order	Clostridiales	family	Ruminococcaceae	genus	Oscillibacter


