#!/usr/bin/env python

import pandas as pd
import sys

datadir = sys.argv[1] # big data frame
headdir = sys.argv[2] # big header file
savedir = sys.argv[3] # where to put finished data frame

data_clv = pd.read_csv(datadir, sep='\t', header=None)
header = [line.rstrip('\n') for line in open(headdir)]
data_clv.columns = header
data_clv['disease'] = data_clv['num'] > 0
data_clv = data_clv.dropna(axis=1)

corr = data_clv.corr()['disease'].abs().sort_values().dropna()
data_clv = data_clv[corr[-17:].index]
data_clv

data_clv.to_csv(savedir, sep='\t')

