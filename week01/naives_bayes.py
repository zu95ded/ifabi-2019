# -*- coding: utf-8 -*-
# open with 
#python3 naives_bayes.py data/processed.cleveland.data

import pandas as pd
from sklearn import metrics
from sklearn.naive_bayes import GaussianNB
from random import sample
from random import seed
import sys



# === PARAMETERS === #
SAMPLE_SIZE = 250
SEED        = 0

if len(sys.argv) == 2:
	CLEVELAND_PATH = sys.argv[1]
	# === LOAD DATA === #

	header = ['age','sex', 'cp', 'trestbps', 'chol', 'fbs', 'restecg', 'thalach', 'exang', 'oldpeak', 'slope', 'ca', 'thal', 'num']
	C = pd.DataFrame(pd.read_csv(CLEVELAND_PATH,header=None))
	C.columns = header
	for h in header:
	    C.loc[:,h] = pd.to_numeric(C.loc[:,h], 'coerce')
	C['num'] = C.loc[:,'num'] > 0
	C=C.dropna()

	seed(SEED)

	REFERENCE = C.T[sample(list(C.index),SAMPLE_SIZE)].T
	CONTROL   = C.T[C.index.difference(sample(list(C.index),SAMPLE_SIZE))].T

	# === MODEL FIT === #

	model = GaussianNB()
	model.fit(REFERENCE[REFERENCE.columns[:-1]], REFERENCE['num'] > 0)
	#expected  = REFERENCE['num'] > 0
	predicted = model.predict(REFERENCE[REFERENCE.columns[:-1]])

	# === VALIDATION === #
	# --- precision recall --- #
	#print(metrics.classification_report(expected, predicted))
	# --- onfusion matrix --- #
	#print(metrics.confusion_matrix(expected, predicted))

	# --- custom prediction --- #
	control_prediction = model.predict(CONTROL[CONTROL.columns[:-1]])
	D=pd.DataFrame([control_prediction,CONTROL['num']]).T
	D.columns=['predict','control']
	TP=sum(D['predict'] & D['control'])
	FN=sum(D['control']) - TP
	FP=sum(D['predict']) - TP
	TN=sum(~D['predict'] & ~D['control'])
	PPV = TP / (TP+FP)
	NPV = TN / (TN+FN)
	# === OUTPUT === #
	print("%d %% of %d randomly chosen control samples were predicted correctly (accuracy)."%(100.0*(TP+TN)/(TP+TN+FN+FP), len(CONTROL.index)))
	print("%d TP, %d TN, %d FN, %d FP; sens = %d %% , spez = %d %%."%(TP,TN,FN,FP,round(100.0*TP/(TP+FN)), round(100.0*TN/(TN+FP))))
else:
	print("ERROR: need path to file processed.cleveland.data")

