import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import sys
import os
from sklearn.svm import SVC
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report, confusion_matrix
from keras.models import Sequential
from keras.layers import Dense

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 

if len(sys.argv) == 2:
    # load data
    CLEVELAND_PATH = sys.argv[1]
    attributes = ['age', 'sex', 'cp', 'trestbps', 'chol', 'fbs', 'restecg', 'thalach', 'exang', 'oldpeak', 'slope', 'ca', 'thal', 'num']
    data_clv = pd.read_csv(CLEVELAND_PATH, sep=',', header=None)
    data_clv.columns = attributes

    # sum up num values 1-4 and remove missing values
    rows_with_missing_values = []
    for index, row in data_clv.iterrows():
        if '?' in list(row):
            rows_with_missing_values.append(index)
        else:
            if row['num'] > 0:
                data_clv.at[index, 'num'] = 1
     
    for idx in rows_with_missing_values:
        data_clv = data_clv.drop(idx)

    # split data into train and test set
    X = data_clv.drop('num', axis=1)
    y = data_clv['num']
    X_train, X_test, y_train, y_test = train_test_split(X, y, train_size = 250, random_state=123)

    # Train SVM classifier
    print('# Train SVM')
    svclassifier = SVC(kernel='linear')
    svclassifier.fit(X_train, y_train)
    SVC(C=1.0, cache_size=200, class_weight=None, coef0=0.0,
        decision_function_shape='ovr', degree=3, gamma='auto_deprecated',
        kernel='linear', max_iter=-1, probability=False, random_state=None,
        shrinking=True, tol=0.001, verbose=False)

    # Evaluate SVM classifier
    y_pred = svclassifier.predict(X_test)
    confu_matrix = confusion_matrix(y_test, y_pred)
    accuracy_svm = (confu_matrix[0,0] + confu_matrix[1,1]) / np.sum(confu_matrix)
    print('Accuracy of SVM classifier: {0}'.format(np.round(accuracy_svm, 2)))

    #Train Neural Network
    print('# Train Neural Network')
    acc = []
    for i in range(100):
        model = Sequential()
        model.add(Dense(20, input_dim=13, activation='relu'))
        model.add(Dense(12, activation='relu'))
        model.add(Dense(1, activation='sigmoid'))
        model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
        model.fit(X_train, y_train, epochs=300, batch_size=10, verbose=0)

        #Evalue Neural Network
        _, accuracy_nn = model.evaluate(X_test, y_test)
        acc.append(accuracy_nn)
    print('Mean Accuracy of Neural Network Classifier: {0}'.format(np.round(np.mean(acc),2)))




    
else:
    print("ERROR: need path to file processed.cleveland.data")