#!/usr/bin/env python
# create data frame containing NaNs
# execute in week folder whihc contains data folder

cat data/cleveland.data \
	| sed 's/ /\t/g' \
	| tr '\n' ' ' \
	| sed 's/name/\n&/g' \
	| sed 's/name //' \
	| head -n 282 \
	| sed 's/-9/NaN/g; s/ /\t/g' \
	> data/cleveland_super.data


cat data/heart-disease.names \
	| head -n 235 \
	| tail -n 109 \
	| sed '/--/d' \
	| sed '79d; 65,68d; 32,44d; 21,22d; 11d' \
	| sed 's/^ *[0-9]*//' \
	| cut -d" " -f2 \
	| sed 's/://g' \
	> data/header_long.txt

python3 create_DF.py data/cleveland_super.data data/header_long.txt data/DF.data

echo "data frame data/DF.data created containing 15 selected features."
